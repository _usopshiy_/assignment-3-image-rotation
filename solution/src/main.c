#include "error_manager.h"
#include "image.h"
#include "io_manager.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>


const uint8_t REQUIRED_ARGS_COUNT = 4;
const uint8_t INPUT_ARG = 1;
const uint8_t OUTPUT_ARG = 2;
const uint8_t ANGLE_ARG = 3;


int main( int argc, char** argv ) {
    if(argc != REQUIRED_ARGS_COUNT){throw_error(NOT_ENOUGH_ARGS); return 0;}

    char* input_file_path = argv[INPUT_ARG];
    char* output_file_path = argv[OUTPUT_ARG];
    int32_t angle = (int32_t) atoi(argv[ANGLE_ARG]);

    if((angle % 90) != 0) {throw_error(UNSUPPORTED_ANGLE); return 0;}

    struct image img = {.width = 0, .height = 0, .data = NULL};
    enum status program_status;

    program_status = read_bmp_image(input_file_path, &img);
    if( program_status != OK){throw_error(program_status); return 0;};

    program_status = rotate_image(&img, angle);
    if( program_status != OK){throw_error(program_status); return 0;};

    program_status = write_bmp_image(output_file_path, &img);
    if( program_status != OK){throw_error(program_status); return 0;};

}
