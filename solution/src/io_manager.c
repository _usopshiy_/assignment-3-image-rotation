#include "io_manager.h"
#include "bmp.h"

static const char* mode_strings[18] = {
    [READ] = "r",
    [WRITE] = "w",
    [APPEND] = "a",
    [READ_BINARY] = "rb",
    [WRITE_BINARY] = "wb",
    [APPEND_BINARY] = "ab",
    [READ_WRITE] = "r+",
    [CREATE_READ_WRITE] = "w+",
    [APPEND_CREATE_READ_WRITE] = "a+",
    [READ_WRITE_BINARY] = "r+b",
    [CREATE_READ_WRITE_BINARY] = "w+b",
    [APPEND_CREATE_READ_WRITE_BINARY] = "a+b",
    [READ_TEXT] = "rt",
    [WRITE_TEXT] = "wt",
    [APPEND_TEXT] = "at",
    [READ_WRITE_TEXT] = "r+t",
    [CREATE_READ_WRITE_TEXT] = "w+t",
    [APPEND_CREATE_READ_WRITE_TEXT] = "a+t"
}; 


static enum status open_file(const char* path, FILE** file, enum open_file_options mode){
    *file = fopen(path, mode_strings[mode]);

    if(*file == NULL){return FILE_OPEN_ERROR;}
    return OK;
}


static enum status close_file(FILE* file){
    if(fclose(file)){return FILE_CLOSE_ERROR;}
    return OK;
}

enum status read_bmp_image(const char* path, struct image* img){
    FILE* file = NULL;
    enum status read_status;

    if(open_file(path, &file, READ_BINARY) != OK){printf("%s", path); return FILE_OPEN_ERROR;};

    read_status = from_bmp(file, img);
    if(read_status != OK){return read_status;}

    if(close_file(file) != OK){return FILE_CLOSE_ERROR;}

    return OK;
}


enum status write_bmp_image(const char* path, struct image* img){
    FILE* file = NULL;
    enum status write_status;

    if(open_file(path, &file, WRITE_BINARY) != OK){free_image(img); return FILE_OPEN_ERROR;}

    write_status = to_bmp(file, img);
    if(write_status != OK){free_image(img); return write_status;}

    if(close_file(file) != OK){free_image(img); return FILE_CLOSE_ERROR;}

    free_image(img);
    return OK; 
}
