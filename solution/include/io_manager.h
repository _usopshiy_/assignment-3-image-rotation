#pragma once

#include "enums.h"
#include "image.h"
#include <stdio.h>


enum status read_bmp_image(const char* path, struct image* img);
enum status write_bmp_image(const char* path, struct image* img);
