#pragma once

#include "enums.h"
#include "image.h"
#include <stdio.h>



enum status from_bmp(FILE* in, struct image* img);
enum status to_bmp(FILE* out, struct image const* img);
uint32_t calculate_padding(uint64_t img_width);
